package Threads;

import java.io.File;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.ThreadFactory;
import java.util.concurrent.TimeUnit;

import javax.servlet.ServletException;

import org.apache.catalina.LifecycleException;
import org.apache.catalina.startup.Tomcat;

/*
 * Main class for application startup with embedded Tomcat and Threads
 */
public class EmbeddedTomcat {

    public static void main(String[] args) throws LifecycleException, InterruptedException, ServletException {

        String docBase = "src/main/webapp/";

        Tomcat tomcat = new Tomcat();
        String webPort = System.getenv("PORT");
        if (webPort == null || webPort.isEmpty()) {
            webPort = "8080";
        }
        tomcat.setPort(Integer.valueOf(webPort));

        tomcat.addWebapp("/", new File(docBase).getAbsolutePath());
        System.out.println("configuring app with basedir: " + new File("./" + docBase).getAbsolutePath());

        tomcat.start();
        
        
        ScheduledExecutorService executor = Executors.newScheduledThreadPool(4, new ThreadFactory() {
            @Override
            public Thread newThread(Runnable r) {
                Thread t = Executors.defaultThreadFactory().newThread(r);
                t.setDaemon(true);
                return t;
            }
        });
        
        Runnable fileProcess = new FileProcesser();
        long spoolingInterval = 10;
        executor.scheduleAtFixedRate(fileProcess, 0, spoolingInterval, TimeUnit.SECONDS);
        tomcat.getServer().await();
    }

}