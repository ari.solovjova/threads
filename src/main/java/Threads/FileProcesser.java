package Threads;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.StandardCopyOption;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;


/*
 * Runnable class for file copying from one directory to another
 */
public class FileProcesser implements Runnable {
    @Override
    public void run() {
        File readFilesFrom = new File("src/main/resources/from/");
        File copyFilesTo = new File("src/main/resources/to/");

        if (readFilesFrom.isDirectory()) {
            List<File> content = new ArrayList<File>(Arrays.asList(readFilesFrom.listFiles()));

            Iterator<File> iter = content.iterator();
            while (iter.hasNext()) {
                File file = iter.next();
                File newFileLocation = new File(copyFilesTo.toString() + File.separator + file.getName().toString());
                try {
                    Files.copy(file.toPath(), newFileLocation.toPath(), StandardCopyOption.REPLACE_EXISTING);
                    // Remove iterator for safe file deletion
                    iter.remove();
                    // Delete saved source file
                    file.delete();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        } else {

            System.out.println("is not directory");
        }
    }
}
