package Threads;

import java.io.File;
import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;


/*
 * Servlet class to add processed file names with get method
 */
@WebServlet(name = "ServletUrl", urlPatterns = { "/" })
public class ServletUrl extends HttpServlet {
    private static final long serialVersionUID = 1L;

//    Not working
    public String getFolderParam(String param) {
        System.out.println("in");
        String folder = getServletContext().getInitParameter(param);
        System.out.println("got");
        return folder;
    }

    // Method to handle GET method request.
    @Override
    public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String folder = getServletContext().getInitParameter("CopyFilesTo");
        File[] files = new File(folder).listFiles();
        
//        Refresh page after 5 seconds
        response.setIntHeader("Refresh", 5);
        request.setAttribute("processedFiles", files);
        
        getServletContext().getRequestDispatcher("/index.jsp").forward(request, response);
    }

    // Method to handle POST method request.
    public void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        doGet(request, response);
    }
}
