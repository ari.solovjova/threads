<%@page import="java.io.File"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Processed files</title>
</head>
<body>
    <a href="ServletUrl">Processed files:</a>
    
    <%;
        out.println("<br/>");
        File[] processedFiles = (File[]) request.getAttribute("processedFiles");
        if (processedFiles != null) {
            for (File file : processedFiles) {
                out.println(file.toString());
                out.println("<br/>");
            }
        }
    %>
</body>
</html>